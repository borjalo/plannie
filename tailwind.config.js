module.exports = {
  // mode: 'jit',
  content: ["./src/**/*.{html,ts}"],
  darkMode: "media", // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        brand: "#f9a77d",
        gray: {
          light: "#92949c",
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require("@tailwindcss/line-clamp")],
};
