// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url: 'https://plannie-app.web.app/',
  firebase: {
    apiKey: 'AIzaSyAw1a7SmQsyOMcUlIjZiLadlLpEs-v-x9k',
    authDomain: 'plannie-app.firebaseapp.com',
    databaseURL: 'https://plannie-app.firebaseio.com',
    projectId: 'plannie-app',
    storageBucket: 'plannie-app.appspot.com',
    messagingSenderId: '551029677860',
    appId: '1:551029677860:web:46127fdd35ac3e0cf39db1',
    measurementId: 'G-Y84EWWK2CF'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
