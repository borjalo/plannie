import { Category } from './category';
import firebase from 'firebase/compat';
import GeoPoint = firebase.firestore.GeoPoint;
import Timestamp = firebase.firestore.Timestamp;

export interface Plan {
    id?: string;
    title: string;
    short_description: string;
    description: string;
    capacity: number;
    start_date: Timestamp;
    end_date: Timestamp;
    price: number;
    lat: number;
    lng: number;
    place: {
        coordinates: GeoPoint;
        name: string;
    };
    category: Category;
    image: string;
    images: string[];
    code: string;
}
