import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private loading: HTMLIonLoadingElement;

  constructor(
    private loadingController: LoadingController
  ) { }

  async presentLoadingWithOptions(message: string) {
    this.loading = await this.loadingController.create({
      spinner: 'crescent',
      message,
      translucent: true,
    });

    await this.loading.present();
  }

  public async dismiss() {
    await this.loading.dismiss();
  }

  public setContent(newContent) {
    this.loading.message = newContent;
  }
}
