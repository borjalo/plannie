import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { Plan } from '../../interfaces/plan';
import * as geofire from 'geofire-common';
import { collection, doc, Firestore, getDoc, getDocs, query, where } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseDbService {
  private planCollection: AngularFirestoreCollection<Plan>;
  private usersCollection: AngularFirestoreCollection<any>;
  private spanishCategoriesCollection: AngularFirestoreCollection<any>;
  private englishCategoriesCollection: AngularFirestoreCollection<any>;

  constructor(private db: AngularFirestore, private firestore: Firestore) {
    this.planCollection = this.db.collection<Plan>('plansES');
    this.usersCollection = this.db.collection<any>('users');
    this.spanishCategoriesCollection = this.db.collection<any>('categoriesES');
    this.englishCategoriesCollection = this.db.collection<any>('categoriesEN');
  }

  async getPlansByCurrentPosition(latitude: number, longitude: number) {
    const todayInSeconds = Math.ceil(new Date().getTime() / 1000);

    const plans = [];
    // Find plans within 50km of current location
    const center = [latitude, longitude];
    const radiusInM = 50 * 1000;

    // Each item in 'bounds' represents a startAt/endAt pair. We have to issue
    // a separate query for each pair. There can be up to 9 pairs of bounds
    // depending on overlap, but in most cases there are 4.
    const bounds = geofire.geohashQueryBounds(center, radiusInM);
    const promises = [];
    for (const b of bounds) {
      const q = this.db.collection('plansES').ref
        .orderBy('geohash')
        .limit(20)
        .startAt(b[0])
        .endAt(b[1]);

      promises.push(q.get());
    }

    // Collect all the query results together into a single list
    Promise.all(promises)
      .then((snapshots) => {
        const matchingDocs = [];

        for (const snap of snapshots) {
          for (const document of snap.docs) {
            const lat = document.get('lat');
            const lng = document.get('lng');
            const timestampStartDate = document.get('start_date') ? document.get('start_date').seconds : null;

            // We have to filter out a few false positives due to GeoHash
            // accuracy, but most will match
            const distanceInKm = geofire.distanceBetween([lat, lng], center);
            const distanceInM = distanceInKm * 1000;

            // Add docs where distance is less than radius and the start date is after today or without start date
            if ((distanceInM <= radiusInM) && ((todayInSeconds < timestampStartDate) || !timestampStartDate)) {
              matchingDocs.push(document);
            }
          }
        }

        return matchingDocs;
      }).then((matchingDocs) => {
        for (const document of matchingDocs) {
          const plan = document.data();

          plan.id = document.id;

          plans.push(plan);
        }
      }).catch(() => {
        alert('Ha habido un error al obtener los planes');
    });

    return plans;
  }

  async getPlansFiltered(filter) {
    const plansCollectionRef = collection(this.firestore, 'plansES');
    let q = query(plansCollectionRef, where('category.value', '==', filter.category), where('capacity', '<=', filter.capacity));

    if (filter.category === 'all') {
      q = query(plansCollectionRef, where('capacity', '<=', filter.capacity));

      return await getDocs(q);
    }

    return await getDocs(q);
  }

  async getPlan(planId: string): Promise<Plan> {
    const docRef = doc(this.firestore, 'plansES', planId);
    const docSnap = await getDoc(docRef);

    return docSnap.data() as Plan;
  }

  async getCategories() {
    const categories = [];

    const querySnapshot = await getDocs(collection(this.firestore, 'categoriesES'));

    querySnapshot.forEach((document) => {
      categories.push(document.data());
    });

    return categories;
  }
}
