import { Component } from '@angular/core';
import { StatusBar } from '@capacitor/status-bar';
import { SplashScreen } from '@capacitor/splash-screen';
import { getAnalytics, logEvent } from '@angular/fire/analytics';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor() {
    const analytics = getAnalytics();
    logEvent(analytics, 'user_entered');
  }

  async initializeApp() {
    try {
      await StatusBar.setOverlaysWebView({
        overlay: false,
      });
      await SplashScreen.hide();
    } catch (e) {
      console.log(e);
    }
  }
}
