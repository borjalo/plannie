import { Component, OnInit, ViewChild } from '@angular/core';
import { Category } from '../../interfaces/category';
import { FirebaseDbService } from '../../services/firebase-db/firebase-db.service';
import { ModalController } from '@ionic/angular';
import { Plan } from '../../interfaces/plan';
import { filter } from 'lodash';
import * as geofire from 'geofire-common';
import { format, parseISO } from 'date-fns';
import { es as ES_FORMAT } from 'date-fns/locale';
import { IonDatetime } from '@ionic/angular';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.page.html',
  styleUrls: ['./filter.page.scss'],
})
export class FilterPage implements OnInit {
  @ViewChild(IonDatetime) datetime: IonDatetime;
  public selectOptions = {
    header: 'Categoría',
    subHeader: 'Selecciona una categoría',
    translucent: false,
  };
  public categories: Category[];
  public filter = {
    category: 'all',
    capacity: 5,
    price: {
      lower: 0,
      upper: 200,
    },
    distance: 5,
  };
  public es = { locale: ES_FORMAT };
  public formattedDate = format(new Date(), 'iii. dd LLLL', this.es);
  public selectedDate: any;
  public minDate = format(new Date(), 'yyyy-MM-dd');
  public filteredPlans: Plan[] = [];

  constructor(
    private firebaseDbService: FirebaseDbService,
    private modalController: ModalController
  ) {}

  async ngOnInit() {
    this.categories = await this.firebaseDbService.getCategories();
  }

  changeDate(value: any) {
    this.selectedDate = value;
    this.formattedDate = format(parseISO(value), 'iii. dd LLLL', this.es);
    this.datetime.confirm(true);
  }

  public applyFilters() {
    this.firebaseDbService
      .getPlansFiltered(this.filter)
      .then(async (results) => {
        results.forEach((doc) => {
          const plan: any = doc.data();

          plan.id = doc.id;
          this.filteredPlans.push(plan as Plan);
        });

        // Filter plans by price
        this.filteredPlans = filter(
          this.filteredPlans,
          (plan: Plan) =>
            this.filter.price.lower <= plan.price &&
            plan.price <= this.filter.price.upper
        );

        // Filter plans by date
        this.filteredPlans = filter(
          this.filteredPlans,
          (plan: Plan) =>
            new Date(this.formattedDate).toDateString() ===
            plan.start_date.toDate().toDateString()
        );

        // Filter plans by distance
        this.filteredPlans = filter(
          this.filteredPlans,
          (plan: Plan) =>
            geofire.distanceBetween(
              [plan.lat, plan.lng],
              [39.544953, -0.427455]
            ) < this.filter.distance
        );

        await this.modalController.dismiss({
          plans: this.filteredPlans,
        });
      });
  }

  public async closeFilters() {
    await this.modalController.dismiss();
  }
}
