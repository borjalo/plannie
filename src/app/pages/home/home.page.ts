import {
  AfterViewInit,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { Plan } from '../../interfaces/plan';
import { FirebaseDbService } from '../../services/firebase-db/firebase-db.service';
import { ModalController, Platform, } from '@ionic/angular';
import { FilterPage } from '../filter/filter.page';
import { GeolocationService } from '../../services/geolocation/geolocation.service';
import { LoadingService } from '../../services/loading/loading.service';
import Swiper, { Pagination, EffectCards, SwiperOptions, Navigation } from 'swiper';
import { environment } from '../../../environments/environment';

Swiper.use([Pagination, Navigation, EffectCards]);

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomePage implements AfterViewInit {
  public plans: Plan[] = [];
  public swiperConfig: SwiperOptions = {
    slidesPerView: 'auto',
    effect: 'cards',
    navigation: true
  };
  public share = false;

  constructor(
    private router: Router,
    private firebaseDbService: FirebaseDbService,
    private plt: Platform,
    private modalController: ModalController,
    private geolocationService: GeolocationService,
    private loadingService: LoadingService
  ) {}

  public async ngAfterViewInit() {
    const gettingCurrentPositionText = 'Obteniendo ubicación actual...';
    await this.loadingService.presentLoadingWithOptions(
      gettingCurrentPositionText
    );
    let position;
    try {
      // position = await this.geolocationService.getCurrentPosition();
      position = {
        coords: {
          lat: 39.453486,
          lng: -0.376524
        }
      };
    } catch (e) {
      console.log(e);
    }

    this.plans = await this.firebaseDbService.getPlansByCurrentPosition(
      position.coords.lat,
      position.coords.lng
    );
    await this.loadingService.dismiss();

    this.plans.sort(() => Math.random() - 0.5);
  }

  public async sharePlan(id: string) {
    const shareLink = `${ environment.url }/plans/${ id }`;

    await navigator.clipboard.writeText(shareLink);

    document.getElementById('link-copied-banner').style.opacity = '100%';

    setTimeout(() => {
      document.getElementById('link-copied-banner').style.opacity = '0';
    }, 4000);
  }

  async openFilters() {
    const filtersModal = await this.modalController.create({
      component: FilterPage,
    });

    await filtersModal.present();

    const { data } = await filtersModal.onDidDismiss();

    if (data) {
      this.plans = data.plans;
    }
  }

  public async goToPlanInfo() {
    await this.router.navigate(['plan-info']);
  }

  public contact() {
    window.open('https://forms.gle/RrqWgCasruDdpk1PA');
    // window.open(
    // 'https://api.whatsapp.com/send?phone=34665987324&text=%C2%A1Hola!%20Estoy%20interesado%20en%20publicitar%20mi%20negocio.'
    // );
  }

  public openLocation(plan: Plan) {
    window.open(
      `https://www.google.com/maps/search/?api=1&query=${ plan.lat }%2C${ plan.lng }`
    );
  }
}
