import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlanInfoPage } from './plan-info.page';

const routes: Routes = [
  {
    path: '',
    component: PlanInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlanInfoPageRoutingModule {}
