import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseDbService } from '../../services/firebase-db/firebase-db.service';
import Swiper, { Autoplay, EffectCoverflow, Pagination, SwiperOptions } from 'swiper';
import { environment } from '../../../environments/environment';
import { Plan } from '../../interfaces/plan';

Swiper.use([Pagination, EffectCoverflow, Autoplay]);

@Component({
  selector: 'app-plan-info',
  templateUrl: './plan-info.page.html',
  styleUrls: ['./plan-info.page.scss'],
})
export class PlanInfoPage implements OnInit {
  public plan: Plan;
  public swiperConfig: SwiperOptions = {
    slidesPerView: 'auto',
    pagination: {
      type: 'progressbar'
    },
    effect: 'coverflow',
    autoplay: {
      delay: 3000,
    },
    speed: 1000
  };
  private planId: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private firebaseDbService: FirebaseDbService,
  ) {}

  async ngOnInit() {
    this.planId = this.activatedRoute.snapshot.paramMap.get('id');

    this.plan = await this.firebaseDbService.getPlan(this.planId);
  }

  public async sharePlan() {
    const shareLink = `${ environment.url }/plans/${ this.planId }`;

    await navigator.clipboard.writeText(shareLink);

    document.getElementById('link-copied-banner').style.opacity = '100%';

    setTimeout(() => {
      document.getElementById('link-copied-banner').style.opacity = '0';
    }, 4000);
  }
}
