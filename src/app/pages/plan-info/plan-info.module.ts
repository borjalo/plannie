import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlanInfoPageRoutingModule } from './plan-info-routing.module';

import { PlanInfoPage } from './plan-info.page';
import { SwiperModule } from 'swiper/angular';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PlanInfoPageRoutingModule,
        SwiperModule
    ],
  declarations: [PlanInfoPage]
})
export class PlanInfoPageModule {}
